package ru.ermolaev.tm.service;

import ru.ermolaev.tm.api.service.IAuthenticationService;
import ru.ermolaev.tm.api.service.IUserService;
import ru.ermolaev.tm.exception.empty.*;
import ru.ermolaev.tm.exception.user.AccessDeniedException;
import ru.ermolaev.tm.model.User;
import ru.ermolaev.tm.util.HashUtil;

public class AuthenticationService implements IAuthenticationService {

    private final IUserService userService;

    private String userId;

    public AuthenticationService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public String getUserId() throws Exception {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuthenticated() {
        return userId != null;
    }

    @Override
    public void login(final String login, final String password) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        final String hash = HashUtil.hidePassword(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void registration(final String login, final String password, final String email) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        userService.create(login, password, email);
    }

    @Override
    public void updatePassword(final String newPassword) throws Exception {
        if (!isAuthenticated()) throw new AccessDeniedException();
        if (newPassword == null || newPassword.isEmpty()) throw new EmptyPasswordException();
        userService.updatePassword(userId, newPassword);
    }

    @Override
    public User showUserProfile() throws Exception {
        if (!isAuthenticated()) throw new AccessDeniedException();
        return userService.findById(userId);
    }

    @Override
    public void updateUserFirstName(final String newFirstName) throws Exception {
        if (!isAuthenticated()) throw new AccessDeniedException();
        if (newFirstName == null || newFirstName.isEmpty()) throw new EmptyFirstNameException();
        userService.updateUserFirstName(userId, newFirstName);
    }

    @Override
    public void updateUserMiddleName(final String newMiddleName) throws Exception {
        if (!isAuthenticated()) throw new AccessDeniedException();
        if (newMiddleName == null || newMiddleName.isEmpty()) throw new EmptyMiddleNameException();
        userService.updateUserMiddleName(userId, newMiddleName);
    }

    @Override
    public void updateUserLastName(final String newLastName) throws Exception {
        if (!isAuthenticated()) throw new AccessDeniedException();
        if (newLastName == null || newLastName.isEmpty()) throw new EmptyLastNameException();
        userService.updateUserLastName(userId, newLastName);
    }

    @Override
    public void updateUserEmail(final String newEmail) throws Exception {
        if (!isAuthenticated()) throw new AccessDeniedException();
        if (newEmail == null || newEmail.isEmpty()) throw new EmptyEmailException();
        userService.updateUserEmail(userId, newEmail);
    }

}
