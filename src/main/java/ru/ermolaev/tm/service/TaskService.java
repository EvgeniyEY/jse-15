package ru.ermolaev.tm.service;

import ru.ermolaev.tm.api.repository.ITaskRepository;
import ru.ermolaev.tm.api.service.ITaskService;
import ru.ermolaev.tm.exception.empty.EmptyIdException;
import ru.ermolaev.tm.exception.empty.EmptyUserIdException;
import ru.ermolaev.tm.exception.incorrect.IncorrectIndexException;
import ru.ermolaev.tm.exception.empty.EmptyNameException;
import ru.ermolaev.tm.model.Task;

import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void createTask(final String userId, final String name) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = new Task();
        task.setName(name);
        taskRepository.add(userId, task);
    }

    @Override
    public void createTask(final String userId, final String name, final String description) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(userId, task);
    }

    @Override
    public void addTask(final String userId, final Task task) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (task == null) return;
        taskRepository.add(userId, task);
    }

    @Override
    public List<Task> showAllTasks(final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return taskRepository.findAll(userId);
    }

    @Override
    public void removeTask(final String userId, final Task task) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (task == null) return;
        taskRepository.remove(userId, task);
    }

    @Override
    public void removeAllTasks(final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        taskRepository.clear(userId);
    }

    @Override
    public Task findTaskById(final String userId, final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.findById(userId, id);
    }

    @Override
    public Task findTaskByIndex(final String userId, final Integer index) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return taskRepository.findByIndex(userId, index);
    }

    @Override
    public Task findTaskByName(final String userId, final String name) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findByName(userId, name);
    }

    @Override
    public Task updateTaskById(
            final String userId,
            final String id,
            final String name,
            final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findTaskById(userId, id);
        if (task == null) return null;
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateTaskByIndex(
            final String userId,
            final Integer index,
            final String name,
            final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findTaskByIndex(userId, index);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task removeTaskById(final String userId, final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.removeById(userId, id);
    }

    @Override
    public Task removeTaskByIndex(final String userId, final Integer index) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return taskRepository.removeByIndex(userId, index);
    }

    @Override
    public Task removeTaskByName(final String userId, final String name) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.removeByName(userId, name);
    }

}
