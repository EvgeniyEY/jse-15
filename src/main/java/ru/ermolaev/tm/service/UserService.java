package ru.ermolaev.tm.service;

import ru.ermolaev.tm.api.repository.IUserRepository;
import ru.ermolaev.tm.api.service.IUserService;
import ru.ermolaev.tm.exception.empty.*;
import ru.ermolaev.tm.model.User;
import ru.ermolaev.tm.role.Role;
import ru.ermolaev.tm.util.HashUtil;

import java.util.List;

public class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id);
    }

    @Override
    public User findByLogin(final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public User findByEmail(final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        return userRepository.findByEmail(email);
    }

    @Override
    public User removeUser(final User user) {
        return userRepository.removeUser(user);
    }

    @Override
    public User removeById(final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.removeById(id);
    }

    @Override
    public User removeByLogin(final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.removeByLogin(login);
    }

    @Override
    public User removeByEmail(final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        return userRepository.removeByEmail(email);
    }

    @Override
    public User create(final String login, final String password) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.hidePassword(password));
        return userRepository.add(user);
    }

    @Override
    public User create(final String login, final String password, final String email) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        final User user = create(login, password);
        if (user == null) return null;
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(final String login, final String password, final Role role) throws Exception {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        final User user = create(login, password);
        if (user == null) return null;
        user.setRole(role);
        return user;
    }

    @Override
    public User updatePassword(final String userId, final String newPassword) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyLoginException();
        if (newPassword == null || newPassword.isEmpty()) throw new EmptyPasswordException();
        final User user = findById(userId);
        user.setPasswordHash(HashUtil.hidePassword(newPassword));
        return user;
    }

    @Override
    public User updateUserFirstName(final String userId, final String newFirstName) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyLoginException();
        if (newFirstName == null || newFirstName.isEmpty()) throw new EmptyFirstNameException();
        final User user = findById(userId);
        user.setFirstName(newFirstName);
        return user;
    }

    @Override
    public User updateUserMiddleName(final String userId, final String newMiddleName) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyLoginException();
        if (newMiddleName == null || newMiddleName.isEmpty()) throw new EmptyMiddleNameException();
        final User user = findById(userId);
        user.setMiddleName(newMiddleName);
        return user;
    }

    @Override
    public User updateUserLastName(final String userId, final String newLastName) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyLoginException();
        if (newLastName == null || newLastName.isEmpty()) throw new EmptyLastNameException();
        final User user = findById(userId);
        user.setLastName(newLastName);
        return user;
    }

    @Override
    public User updateUserEmail(final String userId, final String newEmail) throws Exception {
        if (userId == null || userId.isEmpty()) throw new EmptyLoginException();
        if (newEmail == null || newEmail.isEmpty()) throw new EmptyEmailException();
        final User user = findById(userId);
        user.setEmail(newEmail);
        return user;
    }

}
