package ru.ermolaev.tm.bootstrap;

import ru.ermolaev.tm.api.repository.*;
import ru.ermolaev.tm.api.service.*;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.exception.unknown.UnknownArgumentException;
import ru.ermolaev.tm.exception.unknown.UnknownCommandException;
import ru.ermolaev.tm.exception.empty.EmptyCommandException;
import ru.ermolaev.tm.repository.*;
import ru.ermolaev.tm.role.Role;
import ru.ermolaev.tm.service.*;
import ru.ermolaev.tm.util.TerminalUtil;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Bootstrap implements ServiceLocator {

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthenticationService authenticationService = new AuthenticationService(userService);

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    {
        initCommands(commandService.getCommandList());
    }

    private void initCommands(List<AbstractCommand> commandList) {
        for (AbstractCommand command: commandList) init(command);
    }

    private void init(final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commands.put(command.commandName(), command);
        arguments.put(command.arg(), command);
    }

    private void initUsers() throws Exception {
        userService.create("user", "user", "user@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    public void run(final String[] args) throws Exception {
        System.out.println("Welcome to task manager");
        if (parseArgs(args)) System.exit(0);
        initUsers();
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
            }
        }
    }

    public boolean parseArgs(final String[] args) throws Exception {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    public void parseArg(final String arg) throws Exception {
        if (arg.isEmpty()) return;
        final AbstractCommand argument = arguments.get(arg);
        if (argument == null) throw new UnknownArgumentException(arg);
        argument.execute();
    }

    public void parseCommand(final String cmd) throws Exception {
        if (cmd.isEmpty()) throw new EmptyCommandException();
        final AbstractCommand command = commands.get(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        command.execute();
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthenticationService getAuthenticationService() {
        return authenticationService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

}
