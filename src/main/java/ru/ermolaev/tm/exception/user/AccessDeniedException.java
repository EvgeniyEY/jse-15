package ru.ermolaev.tm.exception.user;

public class AccessDeniedException extends Exception {

    public AccessDeniedException() {
        super("Error! Access denied.");
    }
}
