package ru.ermolaev.tm.exception.empty;

public class EmptyRoleException extends Exception {

    public EmptyRoleException() {
        super("Error! Role is empty.");
    }

}
