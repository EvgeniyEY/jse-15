package ru.ermolaev.tm.exception.empty;

public class EmptyMiddleNameException extends Exception {

    public EmptyMiddleNameException() {
        super("Error! Middle name is empty.");
    }

}
