package ru.ermolaev.tm.exception.empty;

public class EmptyFirstNameException extends Exception {

    public EmptyFirstNameException() {
        super("Error! First name is empty.");
    }

}
