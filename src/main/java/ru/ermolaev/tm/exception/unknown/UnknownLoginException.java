package ru.ermolaev.tm.exception.unknown;

public class UnknownLoginException extends Exception {

    public UnknownLoginException() {
        super("Error! This login does not exist.");
    }

    public UnknownLoginException(final String login) {
        super("Error! This login [" + login + "] does not exist.");
    }

}
