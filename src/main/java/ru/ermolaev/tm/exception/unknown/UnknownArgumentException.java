package ru.ermolaev.tm.exception.unknown;

public class UnknownArgumentException extends Exception {

    public UnknownArgumentException() {
        super("Error! This argument does not exist.");
    }

    public UnknownArgumentException(final String argument) {
        super("Error! This argument [" + argument + "] does not exist.");
    }

}
