package ru.ermolaev.tm.exception.unknown;

public class UnknownCommandException extends Exception {

    public UnknownCommandException() {
        super("Error! This command does not exist.");
    }

    public UnknownCommandException(final String command) {
        super("Error! This command [" + command + "] does not exist.");
    }

}
