package ru.ermolaev.tm.exception.unknown;

public class UnknownIndexException extends Exception {

    public UnknownIndexException() {
        super("Error! This index does not exist.");
    }

    public UnknownIndexException(final Integer index) {
        super("Error! This index [" + index + "] does not exist.");
    }

}
