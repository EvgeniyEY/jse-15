package ru.ermolaev.tm.exception.unknown;

public class UnknownEmailException extends Exception {

    public UnknownEmailException() {
        super("Error! This email does not exist.");
    }

    public UnknownEmailException(final String email) {
        super("Error! This email [" + email + "] does not exist.");
    }

}
