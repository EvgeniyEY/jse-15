package ru.ermolaev.tm.exception.unknown;

public class UnknownNameException extends Exception {

    public UnknownNameException() {
        super("Error! This name does not exist.");
    }

    public UnknownNameException(final String name) {
        super("Error! This name [" + name + "] does not exist.");
    }

}
