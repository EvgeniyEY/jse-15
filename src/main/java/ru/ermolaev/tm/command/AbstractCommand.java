package ru.ermolaev.tm.command;

import ru.ermolaev.tm.api.service.ServiceLocator;

public abstract class AbstractCommand {

    protected ServiceLocator serviceLocator;

    public AbstractCommand() {
    }

    public void setServiceLocator(final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String commandName();

    public abstract String arg();

    public abstract String description();

    public abstract void execute() throws Exception;

}
