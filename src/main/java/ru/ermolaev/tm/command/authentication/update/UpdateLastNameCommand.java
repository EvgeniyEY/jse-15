package ru.ermolaev.tm.command.authentication.update;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.util.TerminalUtil;

public class UpdateLastNameCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "update-last-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update user last name.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE USER LAST NAME]");
        System.out.println("ENTER NEW USER LAST NAME:");
        final String newLastName = TerminalUtil.nextLine();
        serviceLocator.getAuthenticationService().updateUserLastName(newLastName);
        System.out.println("[OK]");
    }

}
