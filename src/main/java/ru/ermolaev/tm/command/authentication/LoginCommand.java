package ru.ermolaev.tm.command.authentication;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.util.TerminalUtil;

public class LoginCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Login in your account.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthenticationService().login(login, password);
        System.out.println("[OK]");
    }

}
