package ru.ermolaev.tm.command.authentication.update;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.util.TerminalUtil;

public class UpdateEmailCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "update-email";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update user e-mail.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE USER EMAIL]");
        System.out.println("ENTER NEW USER EMAIL:");
        final String newEmail = TerminalUtil.nextLine();
        serviceLocator.getAuthenticationService().updateUserEmail(newEmail);
        System.out.println("[OK]");
    }

}
