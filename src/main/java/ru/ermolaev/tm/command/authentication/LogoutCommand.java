package ru.ermolaev.tm.command.authentication;

import ru.ermolaev.tm.command.AbstractCommand;

public class LogoutCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "logout";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Logout from your account.";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        serviceLocator.getAuthenticationService().logout();
        System.out.println("[OK]");
    }

}
