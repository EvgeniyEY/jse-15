package ru.ermolaev.tm.command.authentication.update;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.util.TerminalUtil;

public class UpdateFirstNameCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "update-first-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update user first name.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE USER FIRST NAME]");
        System.out.println("ENTER NEW USER FIRST NAME:");
        final String newFirstName = TerminalUtil.nextLine();
        serviceLocator.getAuthenticationService().updateUserFirstName(newFirstName);
        System.out.println("[OK]");
    }

}
