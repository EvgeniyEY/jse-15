package ru.ermolaev.tm.command.authentication.update;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.util.TerminalUtil;

public class UpdatePasswordCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "update-password";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update user password.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        final String newPassword = TerminalUtil.nextLine();
        serviceLocator.getAuthenticationService().updatePassword(newPassword);
        System.out.println("[PASSWORD CHANGED]");
        serviceLocator.getAuthenticationService().logout();
        System.out.println("[ENTER IN YOUR ACCOUNT AGAIN]");
    }

}
