package ru.ermolaev.tm.command.authentication;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.model.User;

public class ShowProfileCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "show-profile";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show information about user account.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW USER PROFILE]");
        final User user = serviceLocator.getAuthenticationService().showUserProfile();
        System.out.println(user);
        System.out.println("[OK]");
    }

}
