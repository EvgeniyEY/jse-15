package ru.ermolaev.tm.command.project;

import ru.ermolaev.tm.command.AbstractCommand;

public class ClearProjectsCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "project-clear";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Delete all projects.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CLEAR PROJECTS]");
        final String userId = serviceLocator.getAuthenticationService().getUserId();
        serviceLocator.getProjectService().removeAllProjects(userId);
        System.out.println("[COMPLETE]");
    }

}
