package ru.ermolaev.tm.command.project;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.util.TerminalUtil;

public class CreateProjectCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "project-create";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Create a new project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CREATE PROJECT]");
        final String userId = serviceLocator.getAuthenticationService().getUserId();
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        serviceLocator.getProjectService().createProject(userId, name, description);
        System.out.println("[COMPLETE]");
    }

}
