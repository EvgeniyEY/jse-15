package ru.ermolaev.tm.command.task.show;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.model.Task;
import ru.ermolaev.tm.util.TerminalUtil;

public class ShowTaskByNameCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "task-show-by-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show task by name.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER TASK NAME:");
        final String userId = serviceLocator.getAuthenticationService().getUserId();
        final String name = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findTaskByName(userId, name);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[COMPLETE]");
    }

}
