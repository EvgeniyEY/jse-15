package ru.ermolaev.tm.command.task.update;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.model.Task;
import ru.ermolaev.tm.util.TerminalUtil;

public class UpdateTaskByIndexCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "task-update-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update task by index.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER TASK INDEX:");
        final String userId = serviceLocator.getAuthenticationService().getUserId();
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = serviceLocator.getTaskService().findTaskByIndex(userId, index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NEW TASK NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER NEW TASK DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = serviceLocator.getTaskService().updateTaskByIndex(userId, index, name, description);
        if (taskUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[COMPLETE]");
    }

}
