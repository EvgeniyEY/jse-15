package ru.ermolaev.tm.command.task;

import ru.ermolaev.tm.command.AbstractCommand;

public class ClearTasksCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "task-clear";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Delete all tasks.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CLEAR TASKS]");
        final String userId = serviceLocator.getAuthenticationService().getUserId();
        serviceLocator.getTaskService().removeAllTasks(userId);
        System.out.println("[COMPLETE]");
    }

}
