package ru.ermolaev.tm.command.task.update;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.model.Task;
import ru.ermolaev.tm.util.TerminalUtil;

public class UpdateTaskByIdCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "task-update-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Update task by id.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER TASK ID:");
        final String userId = serviceLocator.getAuthenticationService().getUserId();
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findTaskById(userId, id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NEW TASK NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER NEW TASK DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = serviceLocator.getTaskService().updateTaskById(userId, id, name, description);
        if (taskUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[COMPLETE]");
    }

}
