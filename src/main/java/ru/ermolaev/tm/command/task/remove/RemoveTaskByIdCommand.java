package ru.ermolaev.tm.command.task.remove;

import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.model.Task;
import ru.ermolaev.tm.util.TerminalUtil;

public class RemoveTaskByIdCommand extends AbstractCommand {

    @Override
    public String commandName() {
        return "task-remove-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove task by id.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER TASK ID:");
        final String userId = serviceLocator.getAuthenticationService().getUserId();
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().removeTaskById(userId, id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[COMPLETE]");
    }

}
