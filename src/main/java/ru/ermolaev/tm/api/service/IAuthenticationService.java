package ru.ermolaev.tm.api.service;

import ru.ermolaev.tm.model.User;

public interface IAuthenticationService {

    String getUserId() throws Exception;

    boolean isAuthenticated();

    void login(String login, String password) throws Exception;

    void logout();

    void registration(String login, String password, String email) throws Exception;

    void updatePassword(String newPassword) throws Exception;

    User showUserProfile() throws Exception;

    void updateUserFirstName(String newFirstName) throws Exception;

    void updateUserMiddleName(String newMiddleName) throws Exception;

    void updateUserLastName(String newLastName) throws Exception;

    void updateUserEmail(String newEmail) throws Exception;

}
