package ru.ermolaev.tm.repository;

import ru.ermolaev.tm.api.repository.ITaskRepository;
import ru.ermolaev.tm.exception.unknown.UnknownIdException;
import ru.ermolaev.tm.exception.unknown.UnknownIndexException;
import ru.ermolaev.tm.exception.unknown.UnknownNameException;
import ru.ermolaev.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public void add(final String userId, final Task task) {
        task.setUserId(userId);
        tasks.add(task);
    }

    @Override
    public List<Task> findAll(final String userId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task: tasks) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

    @Override
    public void remove(final String userId, final Task task) {
        final List<Task> result = new ArrayList<>();
        for (final Task iter: tasks) {
            if (userId.equals(iter.getUserId())) result.add(iter);
        }
        result.remove(task);
    }

    @Override
    public void clear(final String userId) {
        final List<Task> result = new ArrayList<>();
        for (final Task iter: tasks) {
            if (userId.equals(iter.getUserId())) result.add(iter);
        }
        for (final Task iter: result) {
            tasks.remove(iter);
        }
    }

    @Override
    public Task findById(final String userId, final String id) throws Exception {
        for (final Task task: tasks) {
            if (id.equals(task.getId()) && userId.equals(task.getUserId())) return task;
        }
        throw new UnknownIdException(id);
    }

    @Override
    public Task findByIndex(final String userId, final Integer index) throws Exception {
        for (final Task task: tasks) {
            if (userId.equals(task.getUserId())) {
                if (tasks.indexOf(task) == index) return task;
            }
        }
        throw new UnknownIndexException(index);
    }

    @Override
    public Task findByName(final String userId, final String name) throws Exception {
        for (final Task task: tasks) {
            if (name.equals(task.getName()) && userId.equals(task.getUserId())) return task;
        }
        throw new UnknownNameException(name);
    }

    @Override
    public Task removeById(final String userId, final String id) throws Exception {
        final Task task = findById(userId, id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByIndex(final String userId, final Integer index) throws Exception {
        final Task task = findByIndex(userId, index);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByName(final String userId, final String name) throws Exception {
        final Task task = findByName(userId, name);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

}
