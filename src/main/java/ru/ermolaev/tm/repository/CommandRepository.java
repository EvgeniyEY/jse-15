package ru.ermolaev.tm.repository;

import ru.ermolaev.tm.api.repository.ICommandRepository;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.command.authentication.LoginCommand;
import ru.ermolaev.tm.command.authentication.LogoutCommand;
import ru.ermolaev.tm.command.authentication.RegistrationCommand;
import ru.ermolaev.tm.command.authentication.ShowProfileCommand;
import ru.ermolaev.tm.command.authentication.update.*;
import ru.ermolaev.tm.command.project.ClearProjectsCommand;
import ru.ermolaev.tm.command.project.CreateProjectCommand;
import ru.ermolaev.tm.command.project.remove.RemoveProjectByIdCommand;
import ru.ermolaev.tm.command.project.remove.RemoveProjectByIndexCommand;
import ru.ermolaev.tm.command.project.remove.RemoveProjectByNameCommand;
import ru.ermolaev.tm.command.project.show.ShowProjectByIdCommand;
import ru.ermolaev.tm.command.project.show.ShowProjectByIndexCommand;
import ru.ermolaev.tm.command.project.show.ShowProjectByNameCommand;
import ru.ermolaev.tm.command.project.show.ShowProjectListCommand;
import ru.ermolaev.tm.command.project.update.UpdateProjectByIdCommand;
import ru.ermolaev.tm.command.project.update.UpdateProjectByIndexCommand;
import ru.ermolaev.tm.command.system.*;
import ru.ermolaev.tm.command.task.ClearTasksCommand;
import ru.ermolaev.tm.command.task.CreateTaskCommand;
import ru.ermolaev.tm.command.task.remove.RemoveTaskByIdCommand;
import ru.ermolaev.tm.command.task.remove.RemoveTaskByIndexCommand;
import ru.ermolaev.tm.command.task.remove.RemoveTaskByNameCommand;
import ru.ermolaev.tm.command.task.show.ShowTaskByIdCommand;
import ru.ermolaev.tm.command.task.show.ShowTaskByIndexCommand;
import ru.ermolaev.tm.command.task.show.ShowTaskByNameCommand;
import ru.ermolaev.tm.command.task.show.ShowTaskListCommand;
import ru.ermolaev.tm.command.task.update.UpdateTaskByIdCommand;
import ru.ermolaev.tm.command.task.update.UpdateTaskByIndexCommand;

import java.util.ArrayList;
import java.util.List;

public class CommandRepository implements ICommandRepository {

    private final List<AbstractCommand> commandList = new ArrayList<>();

    {
        commandList.add(new HelpCommand());
        commandList.add(new AboutCommand());
        commandList.add(new SystemInfoCommand());
        commandList.add(new VersionCommand());
        commandList.add(new CommandsCommand());
        commandList.add(new ArgumentsCommand());
        commandList.add(new ExitCommand());

        commandList.add(new LoginCommand());
        commandList.add(new LogoutCommand());
        commandList.add(new RegistrationCommand());
        commandList.add(new ShowProfileCommand());
        commandList.add(new UpdateEmailCommand());
        commandList.add(new UpdateFirstNameCommand());
        commandList.add(new UpdateMiddleNameCommand());
        commandList.add(new UpdateLastNameCommand());
        commandList.add(new UpdatePasswordCommand());

        commandList.add(new CreateTaskCommand());
        commandList.add(new ClearTasksCommand());
        commandList.add(new ShowTaskListCommand());
        commandList.add(new ShowTaskByIdCommand());
        commandList.add(new ShowTaskByIndexCommand());
        commandList.add(new ShowTaskByNameCommand());
        commandList.add(new RemoveTaskByIdCommand());
        commandList.add(new RemoveTaskByIndexCommand());
        commandList.add(new RemoveTaskByNameCommand());
        commandList.add(new UpdateTaskByIdCommand());
        commandList.add(new UpdateTaskByIndexCommand());

        commandList.add(new CreateProjectCommand());
        commandList.add(new ClearProjectsCommand());
        commandList.add(new ShowProjectListCommand());
        commandList.add(new ShowProjectByIdCommand());
        commandList.add(new ShowProjectByIndexCommand());
        commandList.add(new ShowProjectByNameCommand());
        commandList.add(new RemoveProjectByIdCommand());
        commandList.add(new RemoveProjectByIndexCommand());
        commandList.add(new RemoveProjectByNameCommand());
        commandList.add(new UpdateProjectByIdCommand());
        commandList.add(new UpdateProjectByIndexCommand());
    }

    @Override
    public List<AbstractCommand> getCommandList() {
        return commandList;
    }

}
